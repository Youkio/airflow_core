from unittest import TestCase


class Test(TestCase):
    def test_hello_world(self):

        from dags.bamboo import hello_world
        self.assertEqual("hello_world", hello_world())

    # # should raise ERROR
    # def test_hello_world_with_error(self):
    #
    #     from dags.bamboo import hello_world
    #     self.assertEqual("hello_world_2", hello_world())