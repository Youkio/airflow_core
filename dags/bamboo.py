from airflow.models import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from airflow_core.libs.sklearn import example_sklearn


def hello_world():
    return "hello_world"


args = {
    'owner': 'Airflow',
    'start_date': days_ago(1),
}

with  DAG(
        dag_id='hello_world_from_bamboo',
        default_args=args,
        schedule_interval='@once',
        tags=['example']
) as dag:
    hello_dummy = DummyOperator(
        task_id='hello_dummy'
    )

    hello_python = PythonOperator(
        task_id='hello_python',
        python_callable=example_sklearn
    )
