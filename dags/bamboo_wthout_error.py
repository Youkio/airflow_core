from builtins import range
from datetime import timedelta

from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.dates import days_ago

args = {
    'owner': 'Airflow',
    'start_date': days_ago(1),
}

with  DAG(
    dag_id='hello_world_with_error',
    default_args=args,
    schedule_interval='@once',
    tags=['example']
) as dag:
    run_this_last = DummyOperator(
        task_id='hello',
        dag=dag,
    )
    #run_this_last >> run_this_last_2